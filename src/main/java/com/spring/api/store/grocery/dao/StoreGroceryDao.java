package com.spring.api.store.grocery.dao;

import vo.Products;

import java.util.List;

public interface StoreGroceryDao {

    public List<Products> getProductList();
}
