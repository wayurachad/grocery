package com.spring.api.store.grocery.controller;

import com.spring.api.store.grocery.service.StoreGroceryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vo.Products;

import java.util.List;


@RestController
@RequestMapping("/Store/Grocery")
public class StoreGroceryController {
    @Autowired
    StoreGroceryService storeGroceryService;
    @GetMapping("/getProductList")
    public List<Products> getProductList(){
        List<Products> products = storeGroceryService.getProductList();
        return products;
    }

    @PostMapping("/deleteProductList/{productId}")
    public String deleteProductList(){
        return "Delete Success";
    }



}
