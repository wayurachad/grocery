package com.spring.api.store.grocery.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import vo.Products;

import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class StoreGroceryDaoImpl implements StoreGroceryDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Products> getProductList() {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT id AS PRODUCT_ID, ");
        sql.append("product_name AS PRODUCT_NAME ");
        sql.append("FROM PRODUCT");

        List<Products> productList = jdbcTemplate. query(sql.toString(), new ProductsRowMapper());
        return productList;
    }

    private static class ProductsRowMapper implements RowMapper<Products> {
        @Override
        public Products mapRow(ResultSet rs, int rowNum) throws SQLException {
            Products products = new Products();
            products.setId(rs.getInt("PRODUCT_ID"));
            products.setProductName(rs.getString("PRODUCT_NAME"));
            return products;
        }
    }
}
