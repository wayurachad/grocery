package com.spring.api.store.grocery.service;

import org.springframework.stereotype.Service;
import vo.Products;

import java.util.List;

public interface StoreGroceryService {

    public List<Products> getProductList();
}
