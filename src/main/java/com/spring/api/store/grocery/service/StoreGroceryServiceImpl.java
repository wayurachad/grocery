package com.spring.api.store.grocery.service;

import com.spring.api.store.grocery.dao.StoreGroceryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vo.Products;

import java.util.List;

@Service
public class StoreGroceryServiceImpl implements StoreGroceryService{
    @Autowired
    private StoreGroceryDao storeGroceryDao;

    @Override
    public List<Products> getProductList() {
        return storeGroceryDao.getProductList();
    }
}
